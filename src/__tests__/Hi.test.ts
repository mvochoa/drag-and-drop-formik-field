import { Hi } from '../Hi';

test("Test function Hi", ()=>{
    expect(Hi("World")).toEqual("Hi World");
});